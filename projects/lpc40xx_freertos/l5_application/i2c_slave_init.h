#pragma once
#include "FreeRTOS.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

void i2c__slave_init(uint8_t slave_address_to_respond_to);