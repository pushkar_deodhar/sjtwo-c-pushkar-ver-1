#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "i2c_slave_functions.h"
#include "i2c_slave_init.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

void i2c__slave_init(uint8_t);
static volatile uint8_t slave_memory[256];

bool i2c_slave_callback__read_memory(uint8_t memory_index, uint8_t *memory) {
  // TODO: Read the data from slave_memory[memory_index] to *memory pointer
  // TODO: return true if all is well (memory index is within bounds)
  *memory = slave_memory[memory_index];
  
  printf("<=== Slave memory data %d from index %d\n", *memory, memory_index);
  return true;
}

bool i2c_slave_callback__write_memory(uint8_t memory_index, uint8_t memory_value) {
  // TODO: Write the memory_value at slave_memory[memory_index]
  // TODO: return true if memory_index is within bounds
  if (memory_index >= 256)
    return false;
  slave_memory[memory_index] = memory_value;
  printf("<=== Written Index %d with %d ===>\n", memory_index, memory_value);
  return true;
}

int main(void) {
  i2c__slave_init(0x86);

  /**
   * Note: When another Master interacts with us, it will invoke the I2C interrupt
   *.      which will then invoke our i2c_slave_callbacks_*() from above
   *       And thus, we only need to react to the changes of memory
   */
  while (1) {
    // if (slave_memory[0] == 0) {
    //   turn_on_an_led(); // TODO
    // } else {
    //   turn_off_an_led(); // TODO
  }

  // of course, your slave should be more creative than a simple LED on/off

  return -1;
}
